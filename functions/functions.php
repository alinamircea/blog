<?php
session_start();
$mysqli = mysqli_connect("188.240.210.8", "root", "scoalait123", "web-05-red");


function readMySQL($table){
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'`;');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function readCSV($fileName)
{
    $fileLines = file($fileName);
    $data = [];
    $header = str_getcsv($fileLines[0]);
    unset($fileLines[0]);

    foreach ($fileLines as $fileLine) {
        $data[] = array_combine($header, str_getcsv($fileLine));
    }

    return $data;
}

function search($items, $key, $value)
{
    $result = [];
    foreach ($items as $item){
        if ($value == $item[$key]){
            $result[]=$item;
        }
    }

    return $result;
}

function searchMySQL($table, $column, $value)
{
    global $mysqli;
    $query = mysqli_query($mysqli, 'SELECT * FROM `'.$table.'` WHERE '.$column.'="'.$value.'";');

    return $query->fetch_all(MYSQLI_ASSOC);
}

function delete($table, $id)
{
    global $mysqli;
    $query = mysqli_query($mysqli, "DELETE FROM `$table` WHERE id=".intval($id));
}

function insert($table, $data)
{
    global $mysqli;
    $columns =[];
    $values =[];
    foreach ($data as $column=>$value){
        $columns[]=mysqli_real_escape_string($mysqli, $column);
        $values[]=mysqli_real_escape_string($mysqli, $value);
    }
    $columnsList = implode('`, `', $columns);
    $valuesList = implode("','", $values);

    $query = mysqli_query($mysqli, "INSERT INTO `$table` (`$columnsList`) VALUES ('$valuesList');");

    return mysqli_insert_id($mysqli);
}

function update($table, $data, $id)
{
    global $mysqli;
    $sets = [];
    foreach ($data as $column=>$value){
        $sets[]=mysqli_real_escape_string($mysqli, $column)."`='".mysqli_real_escape_string($mysqli, $value);
    }
    $setsList = implode("',`", $sets);

    $query = mysqli_query($mysqli, "UPDATE `$table` SET `$setsList' WHERE id=".intval($id).";");
}

function isLoggedin(){
    return isset($_SESSION['user_id']);
}