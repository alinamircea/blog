<?php


class Category extends BaseTable
{
    protected $name;



    static function getTable()
    {
        return 'categories';
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return mixed
     */
    public function getBgColor()
    {
        return $this->bgColor;
    }

    /**
     * @return mixed
     */


    public function getArticles()
    {
        return Article::findBy(['category_id'=>$this->id]);
    }
}