
    <footer class="footer-distributed">

        <div class="footer-left">

            <div><img class="logo" src="images/red-team-logo.svg"/></div>

            <p class="footer-links">
                <a href="#" class="link-1">Home</a>

                <a href="#">Contact</a>

                <a href="#">Categories</a>

            </p>

            <p class="footer-company-name">Red Team © 2020</p>
        </div>

        <div class="footer-center">

            <div>
                <i class="fa fa-map-marker"></i>
                <p><span>Craiova, Dolj</span> Romania</p>
            </div>

            <div>
                <i class="fa fa-phone"></i>
                <p>0766666666</p>
            </div>

            <div>
                <i class="fa fa-envelope"></i>
                <p><a href="mailto:#">redteam@scoaladeit.com</a></p>
            </div>

        </div>

        <div class="footer-right">

            <p class="footer-company-about">
                <span>About the company</span>
                Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
            </p>

            <div class="footer-icons">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
                <a href="#"><i class="fa fa-github"></i></a>

            </div>

        </div>

    </footer>
