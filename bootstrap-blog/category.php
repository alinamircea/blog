<?php
include 'parts/inclusions.php';
$category = Category::find($_GET['id']);

$articles = $category->getArticles();
//var_dump($articles); die;
?>

<!DOCTYPE html>
<html>
<head>
    <?php include 'parts/head-links.php'; ?>

    <title>Red Team - Category</title>
</head>
<body>
<div class="container-fluid bg-dark">
    <div class="container">
        <?php include 'parts/header.php'?>
    </div>
</div>

<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4">Page Heading
                <small>Secondary Text</small>
            </h1>

            <?php foreach ($articles as $key => $article){
              article($key,$article);
            }
            ?>

            <!-- Pagination -->
            <?php pagination('category.php','id',$_GET['id']) ?>
        </div>

        <!-- Sidebar Widgets Column -->
        <?php include 'parts/sidebar.php'; ?>

    </div>


</div>

<?php include 'parts/footer.php'?>

</body>
</html>