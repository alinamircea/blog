
<?php
include 'parts/inclusions.php';
$articles = Article::findBy(['1'=>1]);





?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="parts/style.css">
    <script src="https://use.fontawesome.com/d5e823a11f.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous">

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
    <div class="container-fluid bg-dark">
        <div class="container">
            <?php include 'parts/header.php'?>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <h1 class="my-4">Movie Reviews</h1>

                <!-- Blog Post -->
                <?php foreach ($articles as $key => $article){
                    article($key,$article);
                }
                ?>

                <!-- Pagination -->
                <?php pagination('index.php','','') ?>
            </div>

            <!-- Sidebar Widgets Column -->

            <?php include 'parts/sidebar.php'; ?>
        </div>
        <!-- /.row -->

    </div>

            <?php include 'parts/footer.php'; ?>


</body>
</html>