<section class="section first-section">
    <div class="container-fluid">
        <div class="masonry-blog row">
            <div class="left-side col-4">
                <div class="masonry-box post-media">
                    <img src="upload/blog_masonry_05.jpg" alt="" class="img-fluid mx-auto">
                    <div class="shadoweffect">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-aqua"><a href="blog-category-01.html" title="">Lifestyle</a></span>
                                <h4><a href="single.html" title="">The golden rules you need to know for a positive life</a></h4>
                                <small><a href="single.html" title="">24 July, 2017</a></small>
                                <small><a href="blog-author.html" title="">by Amanda</a></small>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                    </div><!-- end shadow -->
                </div><!-- end post-media -->
            </div><!-- end left-side -->

            <div class="center-side col-4">
                <div class="masonry-box post-media" id="masonry-box">
                    <img src="upload/blog_masonry_05.jpg" alt="" class="img-fluid mx-auto">
                    <div class="shadoweffect">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-green"><a href="blog-category-01.html" title="">Travel</a></span>
                                <h4><a href="single.html" title="">5 places you should see</a></h4>
                                <small><a href="single.html" title="">24 July, 2017</a></small>
                                <small><a href="blog-author.html" title="">by Amanda</a></small>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                    </div><!-- end shadow -->
                </div><!-- end post-media -->

                <div class="masonry-box small-box post-media" id="masonry-box2">
                    <img src="upload/blog_masonry_05.jpg" alt="" class="img-fluid mx-auto">
                    <div class="shadoweffect">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-green"><a href="blog-category-01.html" title="">Travel</a></span>
                                <h4><a href="single.html" title="">Separate your place with exotic hotels</a></h4>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                    </div><!-- end shadow -->
                </div><!-- end post-media -->

                <div class="masonry-box small-box post-media" id="masonry-box2">
                    <img src="upload/blog_masonry_05.jpg" alt="" class="img-fluid mx-auto">
                    <div class="shadoweffect">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-green"><a href="blog-category-01.html" title="">Travel</a></span>
                                <h4><a href="single.html" title="">What you need to know for child health</a></h4>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                    </div><!-- end shadow -->
                </div><!-- end post-media -->
            </div><!-- end left-side -->

            <div class="right-side hidden-md-down col-4">
                <div class="masonry-box post-media">
                    <img src="upload/blog_masonry_05.jpg" alt="" class="img-fluid mx-auto">
                    <div class="shadoweffect">
                        <div class="shadow-desc">
                            <div class="blog-meta">
                                <span class="bg-aqua"><a href="blog-category-01.html" title="">Lifestyle</a></span>
                                <h4><a href="single.html" title="">The rules you need to know for a happy union</a></h4>
                                <small><a href="single.html" title="">03 July, 2017</a></small>
                                <small><a href="blog-author.html" title="">by Jessica</a></small>
                            </div><!-- end meta -->
                        </div><!-- end shadow-desc -->
                    </div><!-- end shadow -->
                </div><!-- end post-media -->
            </div><!-- end right-side -->
        </div><!-- end masonry -->
    </div>
</section>