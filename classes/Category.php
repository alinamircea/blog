<?php


class Category extends BaseTable2
{
    protected $name;


    static function getTable()
    {
        return 'Category';
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    public function getArticles()
    {
        return Article::findBy(['category'=>$this->name]);
    }
}