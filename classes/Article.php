<?php


class Article extends BaseTable2
{
    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    static function getTable()
    {
        return 'Article';
    }
}